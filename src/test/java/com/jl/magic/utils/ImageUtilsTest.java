package com.jl.magic.utils;

import com.jl.magic.enums.AshingType;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageUtilsTest {

    static BufferedImage read;

    @BeforeClass
    public static void init() {
        try {
            read = ImageIO.read(new File(PathUtils.getRootClassPath() + "/me.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void generateVerCode(){
        ImageUtils.generateSecurityCode("/Users/jiangli/vercode.png");
    }

    @Test
    public void pressImgTest() {
        ImageUtils.pressImage(PathUtils.getRootClassPath() + "/me.jpg",
                "/Users/jiangli/me_pressImg.jpg", PathUtils.getRootClassPath() + "/xiaowangzi.jpg",
                100, 100);
    }

    @Test
    public void pressTextTest() {
        ImageUtils.pressText(PathUtils.getRootClassPath() + "/me.jpg",
                "/Users/jiangli/me_pressTest.jpg", "我是小帅哥", "宋体",
                Font.PLAIN, 100, Color.ORANGE, 700, 2336, 0.9f);
    }

    @Test
    public void base64Test() {
        String base64Str = ImageUtils.imgToBase64(new File(PathUtils.getRootClassPath() + "/me.jpg"));
        System.out.println(base64Str);
        ImageUtils.base64ToImg(base64Str, "/Users/jiangli/meBase64.jpg");
    }

    @Test
    public void ashing() {

        try {
            read = ImageUtils.ashing(read, AshingType.Red);
            ImageIO.write(read, "jpg", new File("/Users/jiangli/ashing_red.jpg"));

            read = ImageUtils.ashing(read, AshingType.Green);
            ImageIO.write(read, "jpg", new File("/Users/jiangli/ashing_green.jpg"));

            read = ImageUtils.ashing(read, AshingType.Blue);
            ImageIO.write(read, "jpg", new File("/Users/jiangli/ashing_blue.jpg"));

            read = ImageUtils.ashing(read, AshingType.RgbMax);
            ImageIO.write(read, "jpg", new File("/Users/jiangli/ashing_max.jpg"));

            read = ImageUtils.ashing(read, AshingType.RgbMin);
            ImageIO.write(read, "jpg", new File("/Users/jiangli/ashing_min.jpg"));

            read = ImageUtils.ashing(read, AshingType.RgbAvg);
            ImageIO.write(read, "jpg", new File("/Users/jiangli/ashing_avg.jpg"));

            read = ImageUtils.ashing(read, AshingType.RgbWeight);
            ImageIO.write(read, "jpg", new File("/Users/jiangli/ashing_weight.jpg"));

            read = ImageUtils.ashing(read, AshingType.Gray);
            ImageIO.write(read, "jpg", new File("/Users/jiangli/ashing_gray.jpg"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void extract() throws IOException {
        read = ImageUtils.extract(read);
        ImageIO.write(read, "png", new File("/Users/jiangli/me.png"));
    }

}