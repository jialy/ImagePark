package com.jl.magic.montage;

import com.jl.magic.montage.model.InitSetting;
import com.jl.magic.utils.PathUtils;
import org.junit.Test;

public class MontageClientTest {

    @Test
    public void montage() {
        System.out.println("开始生成蒙太奇图片...");
        long start = System.currentTimeMillis();

        String rootClassPath = PathUtils.getRootClassPath();

        InitSetting initSetting = new InitSetting()
                .setSource(rootClassPath + "/me.jpg")
                .setTarget("/Users/jiangli/me_300000_30.jpg")
                .setTesseraPath("/Users/jiangli/ZData/imageTmp/")
                .setChip(300000)
                .setScaleSide(30)
                .setCompress(false);

        MontageClient montageClient = new MontageClient(initSetting);
        montageClient.build();

        long end = System.currentTimeMillis();
        System.out.println("图片已生成，耗时：" + (end - start));
    }

}