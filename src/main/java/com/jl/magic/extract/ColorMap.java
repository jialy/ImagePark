package com.jl.magic.extract;


import java.util.ArrayList;

public class ColorMap {

    public final ArrayList<PixelCube> pixelCubes = new ArrayList<>();

    public void push(PixelCube pixelCube) {
        pixelCubes.add(pixelCube);
    }

    public int size() {
        return pixelCubes.size();
    }

    public int[][] palette() {
        int size = pixelCubes.size();
        int[][] palette = new int[size][];
        for (int i = 0; i < size; i++) {
            palette[i] = pixelCubes.get(i).average();
        }
        return palette;
    }

    /**
     * 判断color是否在cube里，在则返回cube里所有像素的平均像素；不在则返回最近的平均像素点
     *
     * @param color 原始rgb值
     * @return
     */
    public int[] map(int[] color) {
        int size = pixelCubes.size();
        for (int i = 0; i < size; i++) {
            PixelCube cube = pixelCubes.get(i);
            if (cube.contains(color)) {
                return cube.average();
            }
        }
        return nearest(color);
    }

    /**
     * 查找最近的平均像素点
     *
     * @param color
     * @return
     */
    public int[] nearest(int[] color) {
        double d1 = Double.MAX_VALUE;
        double d2;
        int[] pColor = null;

        int size = pixelCubes.size();
        for (int i = 0; i < size; i++) {
            int[] vbColor = pixelCubes.get(i).average();
            d2 = Math.sqrt(Math.pow(color[0] - vbColor[0], 2)
                    + Math.pow(color[1] - vbColor[1], 2)
                    + Math.pow(color[2] - vbColor[2], 2));
            if (d2 < d1) {
                d1 = d2;
                pColor = vbColor;
            }
        }
        return pColor;
    }
}
