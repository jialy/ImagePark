package com.jl.magic.extract;


import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.Arrays;

public class ColorExtract {

    /**
     * 像素提取质量，>=1；
     * 值越小，提取主题色越精确，性能低；
     * 相反值越大，主题色差异越大，性能高
     */
    private static final int DEFAULT_QUALITY = 10;
    /**
     * 是否忽略白色，忽略rgb分量同时大约250的像素
     */
    private static final boolean DEFAULT_IGNORE_WHITE = true;

    /**
     * @param sourceImage 图片文件
     * @return 主色调RGB数组
     */
    public static int[] getThemeColor(BufferedImage sourceImage) {
        int[][] palette = getPalette(sourceImage, 5);
        if (palette == null) {
            return null;
        }
        return palette[0];
    }

    /**
     * @param sourceImage 图片文件
     * @param quality     质量
     * @param ignoreWhite 是否忽略白色
     * @return 主色调RGB数组
     */
    public static int[] getThemeColor(BufferedImage sourceImage, int quality, boolean ignoreWhite) {
        int[][] palette = getPalette(sourceImage, 5, quality, ignoreWhite);
        if (palette == null) {
            return null;
        }
        return palette[0];
    }

    public static int[][] getPalette(BufferedImage sourceImage, int maxColors) {
        ColorMap colorMap = getColorMap(sourceImage, maxColors);
        if (colorMap == null) {
            return null;
        }
        return colorMap.palette();
    }

    public static int[][] getPalette(BufferedImage sourceImage, int maxColors, int quality, boolean ignoreWhite) {
        ColorMap colorMap = getColorMap(sourceImage, maxColors, quality, ignoreWhite);
        if (colorMap == null) {
            return null;
        }
        return colorMap.palette();
    }

    /**
     * @param sourceImage 图片文件
     * @param colorCount  提取的主题色个数
     * @return
     */
    public static ColorMap getColorMap(BufferedImage sourceImage, int colorCount) {
        return getColorMap(sourceImage, colorCount, DEFAULT_QUALITY, DEFAULT_IGNORE_WHITE);
    }

    /**
     * 获取主题色
     *
     * @param sourceImage 图片文件
     * @param colorCount  提取的主题色个数
     * @param quality     质量，即提取像素跨度
     * @param ignoreWhite 是否忽略白色
     * @return
     */
    public static ColorMap getColorMap(BufferedImage sourceImage,
                                       int colorCount,
                                       int quality,
                                       boolean ignoreWhite) {
        if (colorCount < 2 || colorCount > 20) {
            throw new IllegalArgumentException("主题色的个数应该在 1 ~ 20 个数之间.");
        }
        if (quality < 1) {
            throw new IllegalArgumentException("质量(quality)不能小于1.");
        }

        //获取像素点
        int[][] pixels;
        switch (sourceImage.getType()) {
            case BufferedImage.TYPE_3BYTE_BGR:
            case BufferedImage.TYPE_4BYTE_ABGR:
                pixels = getPixelsFast(sourceImage, quality, ignoreWhite);
                break;
            default:
                pixels = getPixelsSlow(sourceImage, quality, ignoreWhite);
        }

        ColorMap colorMap = MMCQ.quantize(pixels, colorCount);
        return colorMap;
    }

    /**
     * 提取图片像素，适用如下类型的图片
     * <p>
     * BufferedImage.TYPE_3BYTE_BGR
     * BufferedImage.TYPE_4BYTE_ABGR
     *
     * @param sourceImage 图片文件
     * @param quality     分析提取图片像素的跨度，quality=1时，质量最高；默认10；
     *                    值越小质量越高，效率低；相反值越大提取像素越快，但也可能会造成颜色丢失。
     * @param ignoreWhite 是否忽略白色
     * @return
     */
    private static int[][] getPixelsFast(BufferedImage sourceImage,
                                         int quality,
                                         boolean ignoreWhite) {
        DataBufferByte dataBuffer = (DataBufferByte) sourceImage.getRaster().getDataBuffer();
        byte[] pixels = dataBuffer.getData();
        int pixelCount = sourceImage.getWidth() * sourceImage.getHeight();

        int colorDepth;
        int type = sourceImage.getType();
        switch (type) {
            case BufferedImage.TYPE_3BYTE_BGR:
                colorDepth = 3;
                break;
            case BufferedImage.TYPE_4BYTE_ABGR:
                colorDepth = 4;
                break;
            default:
                throw new IllegalArgumentException("未处理的图片类型: " + type);
        }

        int expectedDataLength = pixelCount * colorDepth;
        if (expectedDataLength != pixels.length) {
            throw new IllegalArgumentException("期望的像素数据长度(" + expectedDataLength + ") != 实际像素数据长度("
                    + pixels.length + ")");
        }

        //将rgb值存储到一个数组中,(pixelCount + quality - 1)防止数组出界
        int numRegardedPixels = (pixelCount + quality - 1) / quality;
        int numUsedPixels = 0;

        int[][] pixelArray = new int[numRegardedPixels][];
        int offset, a, r, g, b;

        switch (type) {
            case BufferedImage.TYPE_3BYTE_BGR:
                for (int i = 0; i < pixelCount; i += quality) {
                    offset = i * 3;
                    b = pixels[offset] & 0xFF;
                    g = pixels[offset + 1] & 0xFF;
                    r = pixels[offset + 2] & 0xFF;
                    //如果不为白色
                    if (!(ignoreWhite && r > 250 && g > 250 && b > 250)) {
                        pixelArray[numUsedPixels] = new int[]{r, g, b};
                        numUsedPixels++;
                    }
                }
                break;
            case BufferedImage.TYPE_4BYTE_ABGR:
                for (int i = 0; i < pixelCount; i += quality) {
                    offset = i * 4;
                    a = pixels[offset] & 0xFF;
                    b = pixels[offset + 1] & 0xFF;
                    g = pixels[offset + 2] & 0xFF;
                    r = pixels[offset + 3] & 0xFF;

                    if (a >= 125 && !(ignoreWhite && r > 250 && g > 250 && b > 250)) {
                        pixelArray[numUsedPixels] = new int[]{r, g, b};
                        numUsedPixels++;
                    }
                }
                break;
            default:
                throw new IllegalArgumentException("未处理的图片类型: " + type);
        }
        //删除pixelArray中空值的部分
        return Arrays.copyOfRange(pixelArray, 0, numUsedPixels);
    }

    /**
     * 提取图片像素，通过BufferedImage.getRGB()方法提取，比较慢，但适合大部分颜色类型的图片
     *
     * @param sourceImage 图片文件
     * @param quality     分析提取图片像素的跨度，quality=1时，质量最高；默认10；
     *                    值越小质量越高，效率低；相反值越大提取像素越快，但也可能会造成颜色丢失。
     * @param ignoreWhite 是否忽略白色
     * @return
     */
    private static int[][] getPixelsSlow(BufferedImage sourceImage,
                                         int quality,
                                         boolean ignoreWhite) {
        int width = sourceImage.getWidth();
        int height = sourceImage.getHeight();

        int pixelCount = width * height;

        int numRegardedPixels = (pixelCount + quality - 1) / quality;
        int numUsedPixels = 0;

        int[][] res = new int[numRegardedPixels][];
        int r, g, b;

        for (int i = 0; i < pixelCount; i += quality) {
            int row = i / width;
            int col = i % width;
            int rgb = sourceImage.getRGB(col, row);

            r = (rgb >> 16) & 0xFF;
            g = (rgb >> 8) & 0xFF;
            b = (rgb) & 0xFF;
            if (!(ignoreWhite && r > 250 && g > 250 && b > 250)) {
                res[numUsedPixels] = new int[]{r, g, b};
                numUsedPixels++;
            }
        }

        return Arrays.copyOfRange(res, 0, numUsedPixels);
    }
}
