package com.jl.magic.utils;

/**
 * 颜色比较
 */
public class ColorCompare {


    /**
     * rgb颜色空间距离法
     * 不建议直接使用，因为往往一个通道的一点改变，会导致最后融合在一起的颜色发生巨大变化，而如果三个通道的同时改变，
     * 却只会使最后的明暗发生变化,色调并不会产生巨大变化。
     *
     * @param rgb1
     * @param rgb2
     * @return
     */
    public static double RGB(int[] rgb1, int[] rgb2) {
        return Math.sqrt(Math.pow((rgb1[0] - rgb2[0]), 2) + Math.pow((rgb1[1] - rgb2[1]), 2)
                + Math.pow((rgb1[2] - rgb2[2]), 2));
    }

    /**
     * LAB颜色空间
     * LAB颜色空间是基于人眼对颜色的感知，可以表示人眼所能感受到的所有颜色。L表示明度，A表示红绿色差，B表示蓝黄色差
     *
     * @param rgb1
     * @param rgb2
     * @return
     */
    public static double LAB(int[] rgb1, int[] rgb2) {
        int rAvg = (rgb1[0] + rgb2[0]) / 2;
        int r = rgb1[0] - rgb2[0];
        int g = rgb1[1] - rgb2[1];
        int b = rgb1[2] - rgb2[2];
        return Math.sqrt((2 + rAvg / 256) * Math.pow(r, 2) + 4 * Math.pow(g, 2) + (2 + (255 - rAvg) / 256) * Math.pow(b, 2));
    }

}
