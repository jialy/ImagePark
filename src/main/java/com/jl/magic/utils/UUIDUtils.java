package com.jl.magic.utils;

import java.util.UUID;

public class UUIDUtils {

    public static String upperUuid() {
        return uuid().toUpperCase();
    }

    public static String uuid() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    public static void main(String[] args) {
        System.out.println(upperUuid());
    }

}
