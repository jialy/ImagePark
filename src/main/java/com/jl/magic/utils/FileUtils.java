package com.jl.magic.utils;


import com.jl.magic.enums.ImageType;

import javax.activation.MimetypesFileTypeMap;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {

    public static void buildFile(BufferedImage bfi, String suffix, String fileName) {
        buildFile(bfi, suffix, new File(fileName));
    }

    public static void buildFile(BufferedImage bfi, String suffix, File outFile) {
        try {
            ImageIO.write(bfi, suffix, outFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<File> getFiles(File file) {
        List<File> result = new ArrayList<>();
        getFiles(file, result);
        return result;
    }

    public static List<File> getFiles(String path) {
        File file = new File(path);
        return getFiles(file);
    }

    private static void getFiles(File file, List<File> result) {
        if (!file.exists()) {
            return;
        }
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    getFiles(files[i], result);
                } else {
                    checkAndAdd(files[i], result);
                }
            }
        } else {
            checkAndAdd(file, result);
        }
    }

    private static void checkAndAdd(File file, List<File> result) {
        String contentType = new MimetypesFileTypeMap().getContentType(file);
        ImageType imageType = ImageType.of(contentType);
        if (imageType == null) {
            String name = file.getName();
            String suffix = name.substring(name.lastIndexOf(".") + 1).toLowerCase();
            if (suffix.equals("png")) {
                result.add(file);
            }
            return;
        }
        switch (imageType) {
            case JPEG:
            case GIF:
                result.add(file);
                return;
            default:
                return;
        }
    }

}
