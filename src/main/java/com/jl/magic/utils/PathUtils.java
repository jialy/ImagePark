package com.jl.magic.utils;

import java.io.File;

/**
 * 获取系统各类路径工具类
 *
 * @Author jiangli  2017/9/28
 * @Version v1.0
 */
public class PathUtils {

    /**
     * 获取web项目根目录
     *
     * @return
     */
    public static String getWebRootPath() {
        try {
            String path = PathUtils.class.getResource("/").toURI().getPath();
            return (new File(path)).getParentFile().getParentFile().getCanonicalPath();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取类根目录路径
     *
     * @return
     */
    public static String getRootClassPath() {
        try {
            String path = PathUtils.class.getClassLoader().getResource("").toURI().getPath();
            return (new File(path)).getAbsolutePath();
        } catch (Exception var2) {
            String path = PathUtils.class.getClassLoader().getResource("").getPath();
            return (new File(path)).getAbsolutePath();
        }
    }

}
