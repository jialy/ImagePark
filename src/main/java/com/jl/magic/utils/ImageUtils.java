package com.jl.magic.utils;

import com.jl.magic.enums.AshingType;
import com.jl.magic.enums.ScaleType;

import javax.imageio.*;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.io.*;
import java.util.Base64;
import java.util.Random;

public class ImageUtils {

    private static Random random = new Random();

    public static String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    /**
     * 生成验证码图片
     *
     * @param target 生成的验证码图片
     */
    public static void generateSecurityCode(String target) {
        try {
            int width = 120;
            int height = 50;
            BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics g = image.getGraphics();
            g.setColor(new Color(random(50, 250), random(50, 250), random(50, 250)));
            g.fillRect(0, 0, width, height);
            for (int i = 0; i < 4; i++) {
                g.setColor(new Color(random(50, 180), random(50, 180), random(50, 180)));
                g.setFont(new Font("黑体", Font.PLAIN, 40));
                char c = chars.charAt(random(0, chars.length()));
                g.drawString(String.valueOf(c), 10 + i * 30, random(height - 30, height));
            }
            //画随机线
            for (int i = 0; i < 25; i++) {
                g.setColor(new Color(random(50, 180), random(50, 180), random(50, 180)));
                g.drawLine(random(0, width), random(0, height), random(0, width), random(0, height));
            }
            ImageIO.write(image, "png", new File(target));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static int random(int min, int max) {
        return random.nextInt(max - min) + min;
    }

    /**
     * 添加图片水印
     *
     * @param sourceImg 源图
     * @param targetImg 目标图片
     * @param pressImg  水印图片
     * @param x         水印在源图x坐标
     * @param y         水印在源图y坐标
     */
    public static void pressImage(String sourceImg, String targetImg, String pressImg, int x, int y) {
        try {
            BufferedImage src = ImageIO.read(new File(sourceImg));
            int width = src.getWidth();
            int height = src.getHeight();
            BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = image.createGraphics();
            g.drawImage(src, 0, 0, width, height, null);

            BufferedImage press = ImageIO.read(new File(pressImg));
            int pressWidth = press.getWidth();
            int pressHeight = press.getHeight();
            g.drawImage(press, x, y, pressWidth, pressHeight, null);
            g.dispose();

            ImageIO.write(image, "jpg", new File(targetImg));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 添加文本水印
     *
     * @param sourceImg 源图片
     * @param targetImg 目标图片
     * @param text      待添加文本
     * @param fontName  字体
     * @param fontStyle 字体样式
     * @param fontSize  字体大小
     * @param color     颜色
     * @param x         文本水印x方向偏移量，如果x<0，则居中
     * @param y         文本水印y方向偏移量，如果y<0，则居中
     * @param alpha     文本水印透明度（0~1），0为完全透明，1为完全不透明
     */
    public static void pressText(String sourceImg, String targetImg, String text, String fontName, int fontStyle, int fontSize,
                                 Color color, int x, int y, float alpha) {
        try {
            BufferedImage src = ImageIO.read(new File(sourceImg));
            int width = src.getWidth(null);
            int height = src.getHeight(null);
            BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = image.createGraphics();
            g.drawImage(src, 0, 0, width, height, null);
            Font font = new Font(fontName, fontStyle, fontSize);
            g.setFont(font);
            g.setColor(color);
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, alpha));

            FontRenderContext fontRenderContext = g.getFontRenderContext();
            Rectangle2D stringBounds = font.getStringBounds(text, fontRenderContext);
            double textWidth = stringBounds.getWidth();
            double textHeight = stringBounds.getHeight();
            int posX, posY;
            if (x < 0) {
                posX = (int) ((width - textWidth) / 2);
            } else {
                posX = x;
            }

            if (y < 0) {
                posY = (int) ((height - textHeight) / 2);
            } else {
                posY = y;
            }

            g.drawString(text, posX, posY);
            g.dispose();
            ImageIO.write(image, "jpg", new File(targetImg));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 图片转base64字符串
     *
     * @param file
     * @return
     */
    public static String imgToBase64(File file) {
        try {
            return imgToBase64(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 图片转base64字符串
     *
     * @param is
     * @return
     */
    public static String imgToBase64(FileInputStream is) {
        try {
            byte[] data = new byte[is.available()];
            is.read(data);
            is.close();
            return Base64.getEncoder().encodeToString(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * base64字符串转图片
     *
     * @param imgStr
     * @param targetFile
     * @return
     */
    public static boolean base64ToImg(String imgStr, String targetFile) {
        if (imgStr == null)
            return false;
        byte[] decode = Base64.getDecoder().decode(imgStr);
        try {
            OutputStream os = new FileOutputStream(targetFile);
            os.write(decode);
            os.flush();
            os.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 提取
     *
     * @param image
     * @return
     */
    public static BufferedImage extract(BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();
        BufferedImage bfi = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics graphics = image.getGraphics();
        graphics.drawImage(image, 0, 0, null);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int pixel = image.getRGB(x, y);
                int[] rgb = rgb(pixel);
                int bright = (int) (0.21f * rgb[0] + 0.71f * rgb[1] + 0.08f * rgb[2]);
                pixel = (bright << 24) & 0xff000000 | (pixel & 0x00ffffff);
                bfi.setRGB(x, y, pixel);
            }
        }
        return bfi;
    }

    /**
     * 图片灰化处理
     *
     * @param image
     * @return
     */
    public static BufferedImage ashing(BufferedImage image, AshingType type) {

        int width = image.getWidth();
        int height = image.getHeight();

        switch (type) {
            case Gray:
                BufferedImage bfi = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
                for (int y = 0; y < height; y++) {
                    for (int x = 0; x < width; x++) {
                        bfi.setRGB(x, y, image.getRGB(x, y));
                    }
                }
                return bfi;
            case Red:
            case Green:
            case Blue:
            case RgbMax:
            case RgbMin:
            case RgbAvg:
            case RgbWeight:
                for (int y = 0; y < image.getHeight(); y++) {
                    for (int x = 0; x < image.getWidth(); x++) {
                        int pixel = image.getRGB(x, y);
                        int[] rgb = rgb(pixel);
                        switch (type) {
                            case Red:
                                pixel = replaceChannel(pixel, rgb[0], "r");
                                break;
                            case Green:
                                pixel = replaceChannel(pixel, rgb[1], "g");
                                break;
                            case Blue:
                                pixel = replaceChannel(pixel, rgb[2], "b");
                                break;
                            case RgbMax:
                                int max = Math.max(rgb[0], Math.max(rgb[1], rgb[2]));
                                if (max == rgb[0]) {
                                    pixel = replaceChannel(pixel, rgb[0], "r");
                                } else if (max == rgb[1]) {
                                    pixel = replaceChannel(pixel, rgb[1], "g");
                                } else {
                                    pixel = replaceChannel(pixel, rgb[2], "b");
                                }
                                break;
                            case RgbMin:
                                int min = Math.min(rgb[0], Math.min(rgb[1], rgb[2]));
                                if (min == rgb[0]) {
                                    pixel = replaceChannel(pixel, rgb[0], "r");
                                } else if (min == rgb[1]) {
                                    pixel = replaceChannel(pixel, rgb[1], "g");
                                } else {
                                    pixel = replaceChannel(pixel, rgb[2], "b");
                                }
                                break;
                            case RgbAvg:
                                int avg = (rgb[0] + rgb[1] + rgb[2]) / 3;
                                pixel = replaceChannel(pixel, avg, "avg");
                                break;
                            case RgbWeight:
                                /**
                                 * 0.21f * r + 0.71f * g + 0.08f * b
                                 * 0.30f * r + 0.59f * g + 0.11f * b
                                 */
                                int weight = (int) (0.30f * rgb[0] + 0.59f * rgb[1] + 0.11f * rgb[2]);
                                pixel = replaceChannel(pixel, weight, "weight");
                                break;
                        }
                        image.setRGB(x, y, pixel);
                    }
                }
                return image;
            default:
                throw new RuntimeException("未处理的灰化类型！");
        }
    }

    /**
     * 用rgb某通道值替换其他通道值
     *
     * @param rgb    像素值
     * @param vector rgb某通道值
     * @param type   通道类型
     * @return 返回转换后的通道值
     */
    private static int replaceChannel(int rgb, int vector, String type) {
        if (type.equals("r")) {
            rgb = ((vector << 8) & 0x0000ff00) | (rgb & 0xffff00ff);
            rgb = (vector & 0x000000ff) | (rgb & 0xffffff00);
        } else if (type.equals("g")) {
            rgb = ((vector << 16) & 0x00ff0000) | (rgb & 0xff00ffff);
            rgb = (vector & 0x000000ff) | (rgb & 0xffffff00);
        } else if (type.equals("b")) {
            rgb = ((vector << 16) & 0x00ff0000) | (rgb & 0xff00ffff);
            rgb = ((vector << 8) & 0x0000ff00) | (rgb & 0xffff00ff);
        } else if (type.equals("weight") || type.equals("avg")) {
            rgb = (vector << 16) & 0x00ff0000 | (rgb & 0xff00ffff);
            rgb = (vector << 8) & 0x0000ff00 | (rgb & 0xffff00ff);
            rgb = (vector) & 0x000000ff | (rgb & 0xffffff00);
        } else {
            throw new RuntimeException("通道类型异常！");
        }
        return rgb;
    }

    /**
     * 按图片短边1:1居中裁剪图片
     *
     * @param bfi 源image
     * @return 返回处理后的image
     */
    public static BufferedImage tailor(BufferedImage bfi) {
        int height = bfi.getHeight();
        int width = bfi.getWidth();
        int min = Math.min(width, height);
        //居中裁剪
        int half, x, y;
        if (min == width) {
            half = (height - min) / 2;
            x = 0;
            y = half;
        } else {
            half = (width - min) / 2;
            x = half;
            y = 0;
        }
        BufferedImage ret = bfi.getSubimage(x, y, min, min);
        BufferedImage target = new BufferedImage(min, min, ret.getType());
        Graphics graphics = target.getGraphics();
        graphics.drawImage(ret, 0, 0, null);
        graphics.dispose();
        return target;
    }

    /**
     * 按比例进行缩放图片
     *
     * @param bfi       图片
     * @param rate      缩放比例
     * @param scaleType 缩放方式
     * @return 返回缩放后的image
     */
    public static BufferedImage scale(BufferedImage bfi, float rate, ScaleType scaleType) {
        int type = bfi.getType();
        if (type == 0) {
            type = 5;
        }

        int width = bfi.getWidth();
        int height = bfi.getHeight();

        int newWidth = (int) (width * rate);
        int newHeight = (int) (height * rate);

        BufferedImage bufImage;
        Image image;

        switch (scaleType) {
            case Scale_Default:
                bufImage = new BufferedImage(newWidth, newHeight, type);
                image = bfi.getScaledInstance(newWidth, newHeight, Image.SCALE_DEFAULT);
                Graphics graphics = bufImage.getGraphics();
                graphics.drawImage(image, 0, 0, null);
                graphics.dispose();
                return bufImage;
            case Scale_2D:
                bufImage = new BufferedImage(newWidth, newHeight, type);
                Graphics2D g2d = bufImage.createGraphics();
                bufImage = g2d.getDeviceConfiguration().createCompatibleImage(newWidth, newHeight, Transparency.TRANSLUCENT);
                g2d.dispose();
                g2d = bufImage.createGraphics();
                image = bfi.getScaledInstance(newWidth, newHeight, Image.SCALE_AREA_AVERAGING);
                g2d.drawImage(image, 0, 0, null);
                g2d.dispose();
                return bufImage;
            default:
                throw new RuntimeException("不支持的缩放类型");
        }
    }

    /**
     * 像素值转rgb值
     *
     * @param pixel 像素值
     * @return rgb通道值
     */
    private static int[] rgb(int pixel) {
        int r = (pixel >> 16) & 0x00ff;
        int g = (pixel >> 8) & 0x0000ff;
        int b = pixel & 0x000000ff;
        return new int[]{r, g, b};
    }

    /**
     * 压缩图片
     *
     * @param bfi 待压缩图片
     * @return 压缩后的图片
     */
    public static BufferedImage compress(BufferedImage bfi) {
        //指定写图片的方式为jpg
        ImageWriter imageWriter = ImageIO.getImageWritersByFormatName("jpg").next();
        ImageWriteParam imageWriteParam = new JPEGImageWriteParam(null);
        //执行压缩模式
        imageWriteParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        //指定压缩程度，取值范围0~1
        imageWriteParam.setCompressionQuality(0.2f);
        imageWriteParam.setProgressiveMode(ImageWriteParam.MODE_DISABLED);
        //指定压缩时使用的色彩模式
        ColorModel colorModel = bfi.getColorModel();
        ImageTypeSpecifier imageTypeSpecifier = new ImageTypeSpecifier(colorModel, colorModel.createCompatibleSampleModel(16, 16));
        imageWriteParam.setDestinationType(imageTypeSpecifier);
        try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            imageWriter.reset();
            //必须先执行out，才能write
            imageWriter.setOutput(ImageIO.createImageOutputStream(os));
            IIOImage iioImage = new IIOImage(bfi, null, null);
            //调用write向out流写入数据
            imageWriter.write(null, iioImage, imageWriteParam);
            //将out流转换成bufferedImage
            byte[] bytes = os.toByteArray();
            InputStream is = new ByteArrayInputStream(bytes);
            BufferedImage read = ImageIO.read(is);
            is.close();
            return read;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bfi;
    }

}
