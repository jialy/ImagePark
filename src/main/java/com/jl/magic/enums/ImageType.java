package com.jl.magic.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 图片类型
 */
public enum ImageType {

    JPEG("image/jpeg"),
    GIF("image/gif"),
    PNG("application/octet-stream");

    private static final Map<String, ImageType> Map = new HashMap<>();

    static {
        for (ImageType type : ImageType.values()) {
            Map.put(type.getCode(), type);
        }
    }

    public static ImageType of(String name) {
        return Map.get(name);
    }


    String code;

    ImageType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
