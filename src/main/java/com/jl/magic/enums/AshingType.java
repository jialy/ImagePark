package com.jl.magic.enums;

/**
 * 灰度处理类型
 */
public enum AshingType {

    Red(1),
    Green(2),
    Blue(3),
    RgbMax(4),
    RgbMin(5),
    RgbAvg(6),
    RgbWeight(7),
    Gray(8);

    int code;

    AshingType(int code) {
        this.code = code;
    }
}
