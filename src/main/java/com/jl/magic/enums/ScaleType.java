package com.jl.magic.enums;

/**
 * 图片缩放类型
 */
public enum ScaleType {

    Scale_Default(1),
    Scale_2D(2);

    int type;

    ScaleType(int type) {
        this.type = type;
    }
}
