package com.jl.magic.montage.model;

/**
 * 源图x,y方向等分个数
 */
public class Slice {

    private int x;

    private int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
