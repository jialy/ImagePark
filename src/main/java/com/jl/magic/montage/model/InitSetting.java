package com.jl.magic.montage.model;


public class InitSetting {

    private String source;  //源图片

    private String target;  //目标图片

    private int chip;   //镶元个数

    private String tesseraPath; //镶元图片目录

    private int scaleSide;  //缩放边长，图片会以短边进行缩放，缩放后的短边为该值

    private boolean isCompress = true; //镶元图片是否做压缩处理，默认压缩

    public InitSetting() {
    }

    public String getSource() {
        return source;
    }

    public InitSetting setSource(String source) {
        this.source = source;
        return this;
    }

    public String getTarget() {
        return target;
    }

    public InitSetting setTarget(String target) {
        this.target = target;
        return this;
    }

    public int getChip() {
        return chip;
    }

    public InitSetting setChip(int chip) {
        this.chip = chip;
        return this;
    }

    public String getTesseraPath() {
        return tesseraPath;
    }

    public InitSetting setTesseraPath(String tesseraPath) {
        this.tesseraPath = tesseraPath;
        return this;

    }

    public int getScaleSide() {
        return scaleSide;
    }

    public InitSetting setScaleSide(int scaleSide) {
        this.scaleSide = scaleSide;
        return this;
    }

    public boolean isCompress() {
        return isCompress;
    }

    public InitSetting setCompress(boolean compress) {
        isCompress = compress;
        return this;
    }
}
