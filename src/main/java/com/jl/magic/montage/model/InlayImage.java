package com.jl.magic.montage.model;

import java.awt.image.BufferedImage;

/**
 * 镶嵌图片
 */
public class InlayImage {

    private String no;//编号
    private BufferedImage bufferedImage;
    private int[] rgb;//主色调
    private Double distance;

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public BufferedImage getBufferedImage() {
        return bufferedImage;
    }

    public void setBufferedImage(BufferedImage bufferedImage) {
        this.bufferedImage = bufferedImage;
    }

    public int[] getRgb() {
        return rgb;
    }

    public void setRgb(int[] rgb) {
        this.rgb = rgb;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }
}
