package com.jl.magic.montage.model;

import java.util.List;

public class SubImage {

    /**
     * 切割后的子图在主图中的坐标
     */
    private int x;
    private int y;

    private int[] rgb;

    private List<String> nears; //颜色最接近的5张图片编号

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int[] getRgb() {
        return rgb;
    }

    public void setRgb(int[] rgb) {
        this.rgb = rgb;
    }

    public List<String> getNears() {
        return nears;
    }

    public void setNears(List<String> nears) {
        this.nears = nears;
    }
}
