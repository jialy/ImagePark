package com.jl.magic.montage;

import com.jl.magic.enums.ScaleType;
import com.jl.magic.extract.ColorExtract;
import com.jl.magic.extract.ColorMap;
import com.jl.magic.extract.PixelCube;
import com.jl.magic.montage.model.InitSetting;
import com.jl.magic.montage.model.InlayImage;
import com.jl.magic.montage.model.Slice;
import com.jl.magic.montage.model.SubImage;
import com.jl.magic.utils.ColorCompare;
import com.jl.magic.utils.FileUtils;
import com.jl.magic.utils.ImageUtils;
import com.jl.magic.utils.UUIDUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.*;

public class MontageClient {

    private InitSetting initSetting;

    private BufferedImage source;

    List<SubImage> subImages;

    List<InlayImage> inlayImages;

    private Slice slice;

    public MontageClient(InitSetting initSetting) {
        this.initSetting = initSetting;
    }

    public void build() {

        initialize();

        generate();

    }

    private void initialize() {
        try {
            int chip = initSetting.getChip();

            File file = new File(initSetting.getSource());
            source = ImageIO.read(file);

            int width = source.getWidth();
            int height = source.getHeight();
            slice = buildSlice(chip, width, height);

            subImages = getSlicedImages(source, slice);

            inlayImages = getInlayImages();

        } catch (IOException e) {
            throw new RuntimeException("Error occurred when initialize...");
        }
    }

    private void generate() {
        subImages.stream().forEach(input -> {
            int[] rgb = input.getRgb();
            inlayImages.parallelStream().forEach(fillImg -> {
                int[] rgbTemp = fillImg.getRgb();
                double lab = ColorCompare.LAB(rgb, rgbTemp);
                fillImg.setDistance(lab);
            });
            Collections.sort(inlayImages, Comparator.comparing(InlayImage::getDistance));
            List<String> noList = new ArrayList<>();
            inlayImages.subList(0, 5).forEach(inlay -> noList.add(inlay.getNo()));
            input.setNears(noList);
        });

        Map<String, Integer> times = new HashMap<>();
        inlayImages.forEach(input -> times.put(input.getNo(), 0));

        Map<String, InlayImage> inlayMap = new HashMap<>();
        inlayImages.forEach(input -> inlayMap.put(input.getNo(), input));

        int minSide = initSetting.getScaleSide();
        int targetWidth = slice.getX() * minSide;
        int targetHeight = slice.getY() * minSide;

        BufferedImage target = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_BGR);
        Graphics graphics = target.getGraphics();
        subImages.parallelStream().forEach(input -> {
            int x = (input.getX() - 1) * minSide;
            int y = (input.getY() - 1) * minSide;
            List<String> nears = input.getNears();
            Map<String, Integer> temp = new HashMap<>();
            nears.forEach(near -> temp.put(near, times.get(near)));
            List<Map.Entry<String, Integer>> entryList = new ArrayList<>(temp.entrySet());
            Collections.sort(entryList, Comparator.comparing(Map.Entry::getValue));
            String inlayNo = entryList.get(0).getKey();
            InlayImage inlayImage = inlayMap.get(inlayNo);
            graphics.drawImage(inlayImage.getBufferedImage(), x, y, null);
            times.put(inlayNo, times.get(inlayNo) + 1);
        });
        graphics.dispose();

        BufferedImage scale = ImageUtils.scale(target, 0.5f, ScaleType.Scale_Default);

        FileUtils.buildFile(scale, "jpg", initSetting.getTarget());
    }

    private List<InlayImage> getInlayImages() {
        List<File> files = FileUtils.getFiles(initSetting.getTesseraPath());
        if (files.size() < 10) {
            throw new RuntimeException("镶元图片不得少于10张！");
        }

        int scaleSide = initSetting.getScaleSide();

        List<InlayImage> images = new ArrayList<>();
        files.parallelStream().forEach(input -> {
            try (InputStream is = new FileInputStream(input)) {
                BufferedImage bfi = ImageIO.read(is);
                int width = bfi.getWidth();
                int height = bfi.getHeight();
                int minSide = Math.min(width, height);
                float rate = scaleSide * 1.0f / minSide;
                //缩放图片
                bfi = ImageUtils.scale(bfi, rate, ScaleType.Scale_Default);
                //裁剪图片
                bfi = ImageUtils.tailor(bfi);
                //压缩图片
                if (initSetting.isCompress()) {
                    bfi = ImageUtils.compress(bfi);
                }
                //获取主色调
                ColorMap colorMap = ColorExtract.getColorMap(bfi, 2, 5, true);
                PixelCube pixelCube = colorMap.pixelCubes.get(0);
                int[] rgb = pixelCube.average();

                InlayImage inlayImage = new InlayImage();
                inlayImage.setNo(UUIDUtils.upperUuid());
                inlayImage.setBufferedImage(bfi);
                inlayImage.setRgb(rgb);
                images.add(inlayImage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        return images;
    }

    /**
     * 获取分割后的子图坐标及主色调
     */
    private List<SubImage> getSlicedImages(BufferedImage source, Slice slice) {

        List<SubImage> result = new ArrayList<>();

        int type = source.getType();
        int width = source.getWidth();
        int height = source.getHeight();

        int perW = width / slice.getX();
        int perH = height / slice.getY();

        int[] pixels;
        byte[] pixelArr;

        for (int i = 0; i < slice.getY(); i++) {
            int yStart = i * perH;
            for (int j = 0; j < slice.getX(); j++) {
                int xStart = j * perW;
                BufferedImage image = new BufferedImage(perW, perH, type);
                switch (type) {
                    case BufferedImage.TYPE_3BYTE_BGR:
                    case BufferedImage.TYPE_4BYTE_ABGR:
                        pixelArr = (byte[]) source.getRaster().getDataElements(xStart, yStart, perW, perH, null);
                        image.getRaster().setDataElements(0, 0, perW, perH, pixelArr);
                        break;
                    default:
                        pixels = source.getRGB(xStart, yStart, perW, perH, null, 0, perH);
                        image.setRGB(xStart, yStart, perW, perH, pixels, 0, perH);
                }
                ColorMap colorMap = ColorExtract.getColorMap(image, 5, 2, false);
                PixelCube pixelCube = colorMap.pixelCubes.get(0);
                SubImage subImage = new SubImage();
                subImage.setX(j + 1);
                subImage.setY(i + 1);
                subImage.setRgb(pixelCube.average());
                result.add(subImage);
            }
        }

        return result;
    }

    private Slice buildSlice(int chip, int width, int height) {
        int shortSide = Math.min(width, height);
        int longSide = shortSide == width ? height : width;

        int side = shortSide;
        int per = longSide / side;
        int i = 1;
        while (per * i < chip) {
            i++;
            side = shortSide / i;
            per = longSide / side;
        }

        Slice slice = new Slice();
        if (shortSide == width) {
            slice.setX(i);
            slice.setY(per);
        } else {
            slice.setX(per);
            slice.setY(i);
        }
        return slice;
    }
}
